//
//  ViewController.swift
//  FrameworkCheckProject
//
//  Created by Sumit Jain on 09/11/18.
//  Copyright � 2018 Sumit Jain. All rights reserved.
//

import UIKit
import MyTestFramework

class ViewController: UIViewController {

    @IBOutlet weak var myview: DesignableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        myview.cornerRadius = 50
        myview.borderColor = UIColor.black
        myview.borderWidth = 16

    }


}

